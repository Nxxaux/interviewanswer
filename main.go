package main

import (
	"errors"
	"fmt"
	"sort"
)

type RangeList struct {
	//TODO: implement
	RangeMap map[int]struct{}
}

func (rangeList *RangeList) Add(rangeElement [2]int) error {
	//TODO: implement
	if len(rangeElement)!=2{
		return errors.New("[Add] Invalid input")
	}
	for i:=rangeElement[0];i<rangeElement[1];i++{
		_,ok:=rangeList.RangeMap[i]
		if !ok{
			rangeList.RangeMap[i]=struct{}{}
		}
	}
	return nil
}

func (rangeList *RangeList) Remove(rangeElement [2]int) error {
	//TODO: implement
	if len(rangeElement)!=2{
		return errors.New("[Remove] Invalid input")
	}
	for i:=rangeElement[0];i<rangeElement[1];i++{
		_,ok:=rangeList.RangeMap[i]
		if ok{
			delete(rangeList.RangeMap,i)
		}
	}
	return nil
}

func (rangeList *RangeList) Print() error {
	//TODO: implement
	UnorderedArray:=[]int{}
	for k:=range rangeList.RangeMap{
		UnorderedArray=append(UnorderedArray,k)
	}
	sort.Ints(UnorderedArray)
	for k,v:=range UnorderedArray{
		if k==len(UnorderedArray)-1{
			fmt.Printf("%d",v)
			break
		}
		fmt.Printf("%d,",v)
	}
	fmt.Println("")
	return nil
}

//this is for a test
func main() {
	rl:=&RangeList{make(map[int]struct{},0)}
	rl.Add([2]int{1,5})
	rl.Print()
	rl.Add([2]int{10,20})
	rl.Print()
	rl.Add([2]int{10,21})
	rl.Print()
	rl.Add([2]int{2, 4})
	rl.Print()
	rl.Add([2]int{3, 8})
	rl.Print()
	rl.Remove([2]int{10, 10})
	rl.Print()
	rl.Remove([2]int{10, 11})
	rl.Print()
	rl.Remove([2]int{15, 17})
	rl.Print()
	rl.Remove([2]int{3, 19})
	rl.Print()
}




